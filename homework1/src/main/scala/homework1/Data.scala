//Design Pattern: Composite Tree Structure
//Part Whole Hierarchies:
//    Data Holds Viewer and search which splits into its own attributes
//     Repositories holds nodes which hold more data


//Design Pattern: Singleton
//The root interface acts as the main hub for all the Classes provided
//Ex: whenever the viewer or search are asked to do a certain command
//    The root interface receives all information for both parties
//    and will save all the information in its appropriate class

package homework1


case class Data (
                  search: Search,
                  viewer: Viewer

                )

case class Edges (
                   node: Any
                 )


case class Nodes (
                   name: String


                 )

//
case class Repositories (
                          nodes: Seq[Nodes]

                        )

//Main root interface
case class RootInterface (
                           data: Data
                         )
//Main viewer class, accesses only what the user's account
//Info
case class Viewer (
                    name: String = "empty",
                    url:  String = "empty",
                    login:String = "empty",
                    email:String = "empty",
                    bio: String = "empty",
                    repositories: Repositories
                  )

//Acts as a searching tool to help lookup any other github users
//Projects, users contributed, forks, etc
case class Search (
                    repositoryCount: Int =0,
                    userCount: Int =0,

                    edges: Seq[Edges]
                  )
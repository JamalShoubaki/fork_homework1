package homework1

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s._
import org.json4s.native.JsonParser.parse

import scala.io.Source

object SampleCode {

  def main(args: Array[String]): Unit = {

  System.out.println("test")
  val BASE_GHQL_URL = "https://api.github.com/graphql"
    val temp="{viewer {name url login email }}"
  implicit val formats = DefaultFormats

  val client = HttpClientBuilder.create().build()
  val httpUriRequest = new HttpPost(BASE_GHQL_URL)
  httpUriRequest.addHeader("Authorization", "Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd")
  httpUriRequest.addHeader("Accept", "application/json")
  val gqlReq = new StringEntity("{\"query\":\"" + temp + "\"}" )
  httpUriRequest.setEntity(gqlReq)

  val response = client.execute(httpUriRequest)
  System.out.println("Response:" + response)
  response.getEntity match {
    case null => System.out.println("Response entity is null")
    case x if x != null => {
      val respJson = Source.fromInputStream(x.getContent).getLines.mkString
      System.out.println(respJson)
      val viewer = parse(respJson).extract[RootInterface]
      System.out.println("Name: " + viewer.data.viewer.name + "\n"
        + "URL: " + viewer.data.viewer.url + "\n"
        + "Username: " +viewer.data.viewer.login)
      //System.out.println(write(viewer))
    }
    }
  }
}

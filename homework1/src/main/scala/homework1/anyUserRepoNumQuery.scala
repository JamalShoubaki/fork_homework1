//Jamal Shoubaki
// Query 3: Any User Repository Count\nDescription: This query will
// allow you to see any users PUBLIC repository count
//
//Test 3: Check if when JamalShoubaki is the username, repoCount is 9
package homework1

import java.io.PrintWriter

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s.DefaultFormats
import org.json4s.native.JsonParser.parse
import java.util.Calendar

import scala.io.Source

object anyUserRepoNumQuery {

  def main(args: Array[String]): Unit = {

    //Access API Endpoint (In this case Github GraphQL)
    val BASE_GHQL_URL = "https://api.github.com/graphql"
    val writer= new PrintWriter("Query3.log")
    implicit val formats = DefaultFormats

    val client = HttpClientBuilder.create().build()
    val httpUriRequest = new HttpPost(BASE_GHQL_URL)
    val Bearer ="Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd"

    print("Query 3: Any User Repository Count\nDescription: This query will allow you to see any users PUBLIC repository count\n\n" +
      "Enter Any Github Username: ")
    writer.write(Calendar.getInstance().getTime +" Query 3: Any user Repo Count\n")
    val username: String = scala.io.StdIn.readLine()
    print("\n")

    //Test 4 will make sure the username is not empty
    if (username.isEmpty){
      print("No username was entered. Program ending...")
      writer.write(Calendar.getInstance().getTime + " Test 4 failed: no username was entered\n")
      System.exit(1)
    }


    httpUriRequest.addHeader("Authorization", Bearer)
    httpUriRequest.addHeader("Accept", "application/json")
    //val gqlReq = new StringEntity("{\"query\":\"" +"query{viewer {login repositories(last:"+ numRepos+"){nodes{name}}}}\"}")
    val gqlReq1 = new StringEntity("{\"query\":\"" +"query {search(query: "+ username + ", type: REPOSITORY, first:10){repositoryCount edges{node { ... on Repository { name}}}} }\"}")

    httpUriRequest.setEntity(gqlReq1)

    val response1 = client.execute(httpUriRequest)
    System.out.println("Response:" + response1)

    response1.getEntity match {
      case null => System.out.println("Response entity is null")
      case x if x != null => {
        val respJson = Source.fromInputStream(x.getContent).getLines.mkString
        //System.out.println(respJson)
        val viewer = parse(respJson).extract[RootInterface]
        System.out.println("\nUsername: " + username + "\nRepository Count: " + viewer.data.search.repositoryCount)
        writer.write(Calendar.getInstance().getTime() + " Username" + username + "\nRepo Count: " + viewer.data.search.repositoryCount +"\n")
        def printList(args: TraversableOnce[_]): Unit = {
          args.foreach(println)
        }

        printList(viewer.data.viewer.repositories.nodes)


        //Test 3: Check if when JamalShoubaki is the username, repoCount is 9
        def Test3_showCorrectRepoCount(): Unit ={
          if(username == "JamalShoubaki"){
            val repoCheck: Int = 9
             if(repoCheck.compareTo(viewer.data.search.repositoryCount.toInt) == 0)
               print("\nTest 3 has Passed")
            writer.write(Calendar.getInstance().getTime() +"\nTest 3 passed\n")

          }
        }

        Test3_showCorrectRepoCount()

        writer.close()


      }
    }
  }

}

//Query 4: Check pages user Count
//Description: Will check the pages total user count based on the username you input
//
//Test 4 will make sure the username is not empty
//Test 5: Check if when JamalShoubaki is the username, repoCount is 1

package homework1

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.json4s.DefaultFormats
import org.json4s.native.JsonParser.parse
import java.io.{File, PrintWriter}
import java.util.Calendar

import scala.io.Source

object anyPageUserCountQuery {

  def main(args: Array[String]): Unit = {
    val writer= new PrintWriter(new File("Query4.log"))
    //Access API Endpoint (In this case Github GraphQL)
    val BASE_GHQL_URL = "https://api.github.com/graphql"
    implicit val formats = DefaultFormats

    val client = HttpClientBuilder.create().build()
    val httpUriRequest = new HttpPost(BASE_GHQL_URL)
    val Bearer ="Bearer 17bec296a0a2e71406aaab6379b46c1760e1cfdd"

    print("Query 4: Any Github User Count\nDescription: This query will allow you to see any page PUBLIC user count\n\n" +
      "Enter Any Github Username (ex: lift): ")
    writer.write(Calendar.getInstance().getTime +" Query 4: Any Github User Count\n\n")

    val username: String = scala.io.StdIn.readLine()
    print("\n")


    //Test 4 will make sure the username is not empty
    if (username.isEmpty){
      print("No username was entered. Program ending...")
      writer.write(Calendar.getInstance().getTime +"\nTest 4 failed, no username was entered\n")
      System.exit(1)
    }

    httpUriRequest.addHeader("Authorization", Bearer)
    httpUriRequest.addHeader("Accept", "application/json")
    val gqlReq = new StringEntity("{\"query\":\"" +"query {search(query: "+ username + ", type: USER, first:10){userCount edges{node { ... on Repository { name}}}} }\"}")

    httpUriRequest.setEntity(gqlReq)

    val response1 = client.execute(httpUriRequest)
    System.out.println("Response:" + response1)

    response1.getEntity match {
      case null => System.out.println("Response entity is null")
      case x if x != null => {
        val respJson = Source.fromInputStream(x.getContent).getLines.mkString
        //System.out.println(respJson)
        val viewer = parse(respJson).extract[RootInterface]
        System.out.println("\nUsername: " + username + "\nUser Count: " + viewer.data.search.userCount)

        def printList(args: TraversableOnce[_]): Unit = {
          args.foreach(println)
        }

        printList(viewer.data.viewer.repositories.nodes)
        writer.write(Calendar.getInstance().getTime +" Username: " + username + "   User Count: " + viewer.data.search.userCount)

        //Test 5: Check if when JamalShoubaki is the username, repoCount is 1
        def Test5_showCorrectUserCount(): Unit ={
          if(username == "JamalShoubaki"){
            val userCheck: Int = 1
            if(userCheck.compareTo(viewer.data.search.userCount.toInt) == 0)
              print("\nTest 5 has Passed")
              writer.write("\n" +Calendar.getInstance().getTime +"Test 5 has passed\n")

          }
        }

        Test5_showCorrectUserCount()
        writer.close()

      }
    }
  }
}

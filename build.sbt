name := "untitled"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "org.apache.httpcomponents" % "httpclient" % "4.5.10",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.10.0.pr3",
  "org.json4s" %% "json4s-jackson" % "3.6.7",
  "net.liftweb" %% "lift-json" % "3.3.0",
  "junit" % "junit" % "4.8.1" % Test

)




